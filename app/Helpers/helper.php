<?php

if (!function_exists('_dateIndo')) {
    function _dateIndo($date)
    {
        if (isset($date)) {
            $date = explode(' ', $date)[0];
            $indoMonth = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            list($year, $month, $date) = explode('-', $date);
            return $date . ' ' . $indoMonth[$month - 1] . ' ' . $year;
        } else {
            return "-";
        }
    }
}

if (!function_exists('_dateIndoTime')) {
    function _dateIndoTime($date)
    {
        if (isset($date)) {
            list($date, $time) = explode(' ', $date);
            return _dateIndo($date) . ' ' . $time;
        } else {
            return "-";
        }
    }
}