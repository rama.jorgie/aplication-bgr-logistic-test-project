<?php

namespace App\Http\Controllers;

use App\Models\BarangModel;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BarangController extends Controller
{
    public function barang(){
        // $data = BarangModel::all();
        // return $data;
        return view('layoute-page.barang.create-barang');
    }
    public function cek_kode_barang($id){
        $cek = BarangModel::where('kode_barang',$id)->count();
        if($cek == NULL){
            return '0';
        }else{
            return '1';
        }
    }
    public function create_barang(Request $request){
        // return request()->all();
        $add = new BarangModel();
        $add->kode_barang = $request->kode_barang;
        $add->nama_barang = $request->nama_barang;
        $add->jumlah_barang = $request->jumlah_barang;
        $add->harga_satuan = $request->harga_satuan;
        $add->tanggal = date('d-m-Y');
        $add->save();

        return redirect()->back()->with('berhasil','berhasil');
    }
    public function browse_barang(Request $request){
        $barang = BarangModel::all();
        $data['barang'] = BarangModel::all();
        if ($request->ajax()) {
            return DataTables::of($barang)->addIndexColumn()->addColumn('action', function ($row) {
                $data =
                    '
                        <a href ="/delete-barang/' . $row->id . '" id="btn-hapus" > <span id="hapus" class="badge bg-danger btn-sm" "><i class="fa fa-trash"></i>Delete</span> </a> 
                        <a href = "/edit-form-barang/'.$row->id.'" > <span id="hapus" class="badge bg-warning btn-sm" "><i class="fa fa-trash"></i>Update</span> </a> 
                    ';
                return $data;
            })->rawColumns(['action'])
            ->make(true);
        }
        return view('layoute-page.barang.browse-barang',$data);
    }
    public function delete_barang($id){
        $barang = BarangModel::where('id',$id)->first();
        $barang->delete();
        return redirect()->back()->with('hapus','dihapus');
    }
    public function form_edit_barang($id){
        $data['edit'] = BarangModel::where('id',$id)->first();
        return view('layoute-page.barang.updated-form-barang',$data);
    }
    public function save_update_barang(Request $request){
        // dd($request->all());
        BarangModel::where('id',$request->id)->update([
            'nama_barang' => $request->nama_barang,
            'jumlah_barang' => $request->jumlah_barang,
            'harga_satuan' => $request->harga_satuan,

        ]);
        return redirect()->route('browse-barang')->with('ditambah','add');
        
    }
}
