<?php

namespace App\Http\Controllers;

use App\Models\BarangModel;
use App\Models\MBarang_Model;
use App\Models\Mutasi_Model;
use App\Models\Trx_Mutasi_Kode_Barang_Model;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MutasiController extends Controller
{
    public function create_mutasi(){
        $data['kode_barang'] = BarangModel::all();
        $data['key'] = strtoupper(uniqid());
        return view('layoute-page.mutasi.create-mutasi',$data);
    }

    public function save_kode_barang($id ,$kode){
        // return $id;
        $nama_barang = BarangModel::where('kode_barang',$kode)->first();
        $add = new Trx_Mutasi_Kode_Barang_Model();
        $add->no_bukti = $id;
        $add->kode_barang = $kode;
        $add->nama_barang = $nama_barang->nama_barang;
        $add->save();

        $data = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->get();
       
        foreach ($data as $value) {
            $stock = BarangModel::where('kode_barang',$value->kode_barang)->first();
            $html = '
            <table class="table" style= >
                <tr  style="text-align:center ;">
                    <th>ID</th>
                    <th>Kode</th>
                    <th>Nama Barang</th>
                    <th>Sisa Stock</th>
                    <th>Jumlah Order Barang</th>
                </tr>
                <tr  style="text-align:center ;">
                    <td>'.$value->id.'</td>
                    <td>'.$value->kode_barang. '</td>
                    <td>' . $value->nama_barang . '</td>
                    <td>' . $stock->jumlah_barang . '</td>
                    <td style="text-align:center ;"><input  class="form-control" type="number" min="1" max="'. $stock->jumlah_barang.'" name="jumlah_barang[]" value="1" required></td>
                </tr>
            </table>';
        }
        return $html;
    }
    public function delete_kode_barang($id){
        // return $id;  
        $data = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->first();
        $data->delete();
        // $data = Trx_Mutasi_Kode_Barang_Model::where('no_bukti', $id)->get();
        $html ='';
        return $html;
    }
    public function save_mutasi(Request $request){
        // return $request->all();
        // $data_stok = BarangModel::where
        $data_barang = Trx_Mutasi_Kode_Barang_Model::where('no_bukti', $request->no_bukti)->get();
        // return $data;
        $tagihan =  [];
        
        for ($i=0; $i <count($request->jumlah_barang) ; $i++) { 
        // foreach ($data_barang as $value){
            $stok_barang = BarangModel::where('kode_barang',$data_barang[$i]->kode_barang)->first();
            if ($request->indikator == 'keluar'){
                if($request->jumlah_barang[$i] > $stok_barang->jumlah_barang){
                    return 'jumlah tidak sesuai';
                }
                $sisa = $stok_barang->jumlah_barang - $request->jumlah_barang[$i];
            }
            $tagihan[] = $request->jumlah_barang[$i] * $stok_barang->harga_satuan;
            BarangModel::where('kode_barang', $data_barang[$i]->kode_barang)->update([
                'jumlah_barang' => $sisa,
            ]);
            Trx_Mutasi_Kode_Barang_Model::where('kode_barang', $data_barang[$i]->kode_barang)->where('no_bukti', $request->no_bukti)->update([
                'jumlah_order' =>$request->jumlah_barang[$i],   
            ]);

        // }
        }
        // return array_sum($harga_normal);
        $harga_normal = array_sum($tagihan);
        $pajak = 11;
        $total_pajak = ($pajak / 100) * $harga_normal;
        $add = new Mutasi_Model();
        $add->no_bukti = $request->no_bukti;
        $add->tanggal = $request->tanggal;
        $add->indikator = $request->indikator;
        $add->harga_normal = $harga_normal;
        $add->harga_pajak = $harga_normal + $total_pajak  ;
        $add->out = date('d-m-Y');
        $add->keterangan = $request->keterangan;
        $add->save();
        return redirect()->back()->with('save','save');

    }
    public function report_mutasi(Request $request){
        if ($request->tgl_awal != Null || $request->tgl_akhir != Null){ 
            $data_mutasi = Mutasi_Model::whereBetween('tanggal', [$request->tgl_awal, $request->tgl_akhir])->get();
            foreach ($data_mutasi as $value) {
                $nama_barang = Trx_Mutasi_Kode_Barang_Model::select('nama_barang')->where('no_bukti', $value->no_bukti)->get();
                $barang = [];
                $qty = [];
                for ($i = 0; $i < count($nama_barang); $i++) {
                    $barang[] = $nama_barang[$i]->nama_barang;
                }
                $qty_detail = implode(' || ', $qty);
                $data_barang = implode(' || ', $barang);
                $mutasi[] = [
                    'id' => $value->id,
                    'no_bukti' => $value->no_bukti,
                    'nama_barang' => $data_barang,
                    'tanggal' => $value->tanggal,
                    'indikator' => $value->indikator,
                    'harga_normal' => $value->harga_normal,
                    'harga_pajak' => $value->harga_pajak,
                    'qty' => $qty_detail,
                    'out' => $value->out,
                    'keterangan' => $value->keterangan
                ];
            }
        }else{
            // $mutasi = Mutasi_Model::join('trx_mutasi_kode_barang', 'trx_mutasi_kode_barang.no_bukti','=','m_mutasi.no_bukti')
            // ->select('m_mutasi.no_bukti','m_mutasi.tanggal', )
            // ->groupBy('m_mutasi.no_bukti', 'm_mutasi.tanggal')->get();
            // $mutasi = [];
            $data_mutasi = Mutasi_Model::all();
            $mutasi = [];
            foreach ($data_mutasi as $value){
                $nama_barang = Trx_Mutasi_Kode_Barang_Model::select('nama_barang','jumlah_order')->where('no_bukti',$value->no_bukti)->get();
                $barang = [];
                $qty = [];
                for ($i=0; $i <count($nama_barang) ; $i++) { 
                    $barang[] =$nama_barang[$i]->nama_barang;
                    $qty[] = $nama_barang[$i]->jumlah_order;
                }
                $data_barang = implode(' || ', $barang);
                $qty_detail = implode(' || ', $qty);
                $mutasi[] =[
                    'id' => $value->id,
                    'no_bukti' => $value->no_bukti,
                    'nama_barang' => $data_barang,
                    'tanggal' => $value->tanggal,
                    'indikator' => $value->indikator,
                    'harga_normal' => $value->harga_normal,
                    'harga_pajak' => $value->harga_pajak,
                    'qty' => $qty_detail,
                    'out' => $value->out,
                    'keterangan' => $value->keterangan
                ];
            }

        }
        // dd($mutasi);
        // return $qty_detail;

        $data['trx_kode_barang'] = Trx_Mutasi_Kode_Barang_Model::all();
        $data['mutasi'] = Mutasi_Model::all();
        $data['barang'] = BarangModel::all();
        if ($request->ajax()) {
            return DataTables::of($mutasi)->addIndexColumn()->addColumn('action', function ($row) {
                $data =
                    '
                        <a href ="/delete-mutasi/' . $row['no_bukti'] . '" id="btn-hapus" > <span id="hapus" class="badge bg-danger btn-sm" "><i class="fa fa-trash"></i>Delete</span> </a> 
                        <a href = "/invoice/' . $row['no_bukti'] . '" > <span id="hapus" class="badge bg-primary btn-sm" "><i class="fa fa-trash"></i>Invoice</span> </a> 

                    ';
                
                return $data;
            })->rawColumns(['action'])
            ->make(true);
        }
        return view('layoute-page.mutasi.browse-mutasi',$data);  
    }
    public function get_barang_mutasi($id){
        return 'INI BARANG';
    }

    public function delete_mutasi($id){
        $m_mutasi = Mutasi_Model::where('no_bukti',$id)->first();
        $m_mutasi->delete();
        $trx_mutasi = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->delete();
        return redirect()->back();
    }

    public function update_mutasi($id){
        $data['kode_barang'] = BarangModel::all();

        $data['mutasi'] = Mutasi_Model::where('no_bukti',$id)->first();
        $data['trx_mutasi'] = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->get();
        return view ('layoute-page.mutasi.updated-mutasi',$data);
    }
    public function save_update_mutasi(Request $request){

        Mutasi_Model::where('no_bukti',$request->no_bukti)->update([
            'tanggal' => $request->tanggal,
            'indikator' => $request->indikator,
            'qty' => $request->qty,
            'keterangan' => $request->keterangan,
        ]);

        return redirect()->route('report-mutasi');
    }
    public function delete_item_mutasi($id){
        // return $id;
        $trx_mutasi = Trx_Mutasi_Kode_Barang_Model::where('id',$id)->first();
        $trx_mutasi->delete();
        return redirect()->back();
    }
     
}
