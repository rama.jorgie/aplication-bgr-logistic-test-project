<?php

namespace App\Http\Controllers;

use App\Models\Mutasi_Model;
use App\Models\Trx_Mutasi_Kode_Barang_Model;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    function getRomawi($bln){
        switch ($bln) {
            case 1:

                return "I";

                break;

            case 2:

                return "II";

                break;

            case 3:

                return "III";

                break;

            case 4:

                return "IV";

                break;

            case 5:

                return "V";

                break;

            case 6:

                return "VI";

                break;

            case 7:

                return "VII";

                break;

            case 8:

                return "VIII";

                break;

            case 9:

                return "IX";

                break;

            case 10:

                return "X";

                break;

            case 11:

                return "XI";

                break;

            case 12:

                return "XII";

                break;

     }
    }
    public function get_invoice($id){
        $tgl = Mutasi_Model::where('no_bukti',$id)->first();
        $tgl_exp = explode('-',$tgl->tanggal);
        $lastnumm = Mutasi_Model::orderBy('id','asc')->first();
        // return $lastnumm;
        if($lastnumm->invoice == null){
            $no_urut = '001';
            // return 'awal';
            Mutasi_Model::where('no_bukti',$id)->update([
                'invoice' => 'INV/' . $this->getRomawi($tgl_exp[1]) . '/' . $tgl_exp[0] . '/' . sprintf("%03d", $no_urut)
            ]);

        }else{
            $data_invoice = Mutasi_Model::count();
            $last = Mutasi_Model::latest('created_at')->get();
            $last_invoice = explode('/', $last[$data_invoice - 1]->invoice);
            $no_urut = $last_invoice[3] + 1;
            // return $no_urut;
            Mutasi_Model::where('no_bukti', $id)->update([
                'invoice' => 'INV/' . $this->getRomawi($tgl_exp[1]) . '/' . $tgl_exp[0] . '/' . sprintf("%03d", $no_urut)   
            ]);
        }
        $data['invoice'] = Mutasi_Model::where('no_bukti',$id)->first();
        $data['invoice_detail'] = Trx_Mutasi_Kode_Barang_Model::where('no_bukti', $id)->get();

        return view('layoute-page.invoice.invoice',$data);
    }

    
}
