<?php

namespace App\Http\Controllers;

use App\Models\Accounts_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DataTables;


class AccountsController extends Controller
{
    public function accounts(Request $request){
        $accounts = Accounts_Model::where('level','!=','super_admin')->get();
        
        if ($request->ajax()) {
            return DataTables::of($accounts)->addIndexColumn()->addColumn('action', function ($row) {
               
                    $data =
                        '
                        <a href ="/delete-user/' . $row->id . '" > <span id="hapus" class="badge bg-danger btn-sm" "><i class="fa fa-trash"></i>Delete</span> </a> 
                        ';
                    return $data;  
            })->rawColumns(['action'])
                ->make(true);
        }
        return view('layoute-page.accounts.user-accounts');
    }
    public function save_accounts(Request $request){
        $add = new Accounts_Model();
        $add->level = $request->level;
        $add->username = $request->username;
        $add->password = Crypt::encryptString($request->password);
        $add->save();
        return redirect()->back();
    }
    public function login(Request $request){
        try {
            $cek = Accounts_Model::where('username', $request->username)->first();
            $password = Crypt::decryptString($cek->password);
            if ($request->password == $password) {
                session()->put('username',$request->username);
                session()->put('level',$cek->level);
                
                return redirect()->route('beranda');
            } else {
                return redirect()->back()->with('alert', 'Password Salah');
            }
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('alert', 'Password Salah');
        }
    }
    public function delete_user($id){
        $user = Accounts_Model::where('id',$id)->first();
        $user->delete();
        return redirect()->back();
    }
}
