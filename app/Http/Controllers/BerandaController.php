<?php

namespace App\Http\Controllers;

use App\Models\Accounts_Model;
use App\Models\BarangModel;
use App\Models\Mutasi_Model;
use Illuminate\Http\Request;

class BerandaController extends Controller
{
   public function beranda(){
    $data['barang'] = BarangModel::count();
    $data['mutasi'] = Mutasi_Model::count();
    $data['user'] = Accounts_Model::where('level','!=','super_admin')->count();
    return view('layoute-page.beranda.beranda',$data);
   }
}
