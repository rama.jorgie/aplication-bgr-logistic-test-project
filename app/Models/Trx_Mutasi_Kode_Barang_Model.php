<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trx_Mutasi_Kode_Barang_Model extends Model
{
    protected $table = 'trx_mutasi_kode_barang';
}
