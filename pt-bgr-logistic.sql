/*
 Navicat Premium Data Transfer

 Source Server         : SQL CON
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : pt-wood-integra

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 04/09/2022 20:41:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `level` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES (2, 'admin', 'eyJpdiI6ImlFN0JPR2JuZ2IzejRiYnZCbmEvNVE9PSIsInZhbHVlIjoiTkNkNWE2a0tlWEw0c3NqU2RSZDBxQT09IiwibWFjIjoiYTEwYTIzZTJiMWZhM2M4Zjg2YmEyOTcxZjhiOTlhYmUxNDYwODM5MjYyMzhhOGRmZWUwNWI2ZjA0MjgxZDNkYiIsInRhZyI6IiJ9', 'super_admin', '2022-09-04 08:42:17', '2022-09-04 08:42:17');
INSERT INTO `accounts` VALUES (3, 'admin_stock', 'eyJpdiI6IkErWjFCRmpUaEx4ZUVRcHU2aXV1S0E9PSIsInZhbHVlIjoiWXJ0NUxYVE5VSkF1UmROLzUxUUJXUT09IiwibWFjIjoiZTNkNTY1OTMzM2MxNzJhYmM2OGNjMzJiOTgwNjhjMzkzZTQyNjliNzBjOTQyZWI2ODZmZDUzYzlmNTQ1ZGUxMyIsInRhZyI6IiJ9', 'admin_stock', '2022-09-04 11:03:19', '2022-09-04 11:03:19');
INSERT INTO `accounts` VALUES (4, 'kasir', 'eyJpdiI6ImlSMGJkVWlVRTJRVjhDc2ozdXNWNHc9PSIsInZhbHVlIjoiREVjL0NPa2p3Kys0ZitHOEo2L0dndz09IiwibWFjIjoiYWE4MTcwNGVlNmFkODAwYTFiMTJjZWM5MmQxZTA4ZTJlMjExMGFhNTQyOWM4YWY2NTQzZGQ1MGFjYTJkYjhiYSIsInRhZyI6IiJ9', 'kasir', '2022-09-04 11:03:33', '2022-09-04 11:03:33');

-- ----------------------------
-- Table structure for m_barang
-- ----------------------------
DROP TABLE IF EXISTS `m_barang`;
CREATE TABLE `m_barang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `harga_satuan` double NULL DEFAULT NULL,
  `nama_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_barang
-- ----------------------------
INSERT INTO `m_barang` VALUES (9, 'XHHUH-IUIF-GFUHIB', 1000, 'Detergent', '18', '29-08-2022', '2022-08-29 03:58:57', '2022-09-03 16:15:13');
INSERT INTO `m_barang` VALUES (10, 'XBCV-ADHIJ-DAKJJDA', 2000, 'Sabun Mandi', '93', '03-09-2022', '2022-09-03 15:36:20', '2022-09-03 17:45:05');
INSERT INTO `m_barang` VALUES (11, 'XHA-FAIAF-AFAJAFJF', 3000, 'Beras', '41', '03-09-2022', '2022-09-03 15:39:58', '2022-09-03 16:16:48');
INSERT INTO `m_barang` VALUES (12, 'XYRY-HHBK-BJ', 4000, 'Sapu', '5', '04-09-2022', '2022-09-04 12:59:04', '2022-09-04 13:00:28');
INSERT INTO `m_barang` VALUES (13, 'GFYYUG-HUGYFY-HUGY', 25000, 'GAS ELPIJI', '60', '04-09-2022', '2022-09-04 13:08:42', '2022-09-04 13:10:50');
INSERT INTO `m_barang` VALUES (14, 'NDJAIJ-DAKB-ADADHSA', 8000, 'Kasuur Tidur', '70', '04-09-2022', '2022-09-04 13:28:53', '2022-09-04 13:31:01');

-- ----------------------------
-- Table structure for m_mutasi
-- ----------------------------
DROP TABLE IF EXISTS `m_mutasi`;
CREATE TABLE `m_mutasi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_bukti` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `invoice` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `indikator` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `harga_normal` double NULL DEFAULT NULL,
  `harga_pajak` double NULL DEFAULT NULL,
  `out` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_mutasi
-- ----------------------------
INSERT INTO `m_mutasi` VALUES (25, '63137DDF9D320', 'INV/IX/2022/002', '2022-09-23', 'keluar', 30000, 33300, '03-09-2022', 'damdkajda', '2022-09-03 16:16:48', '2022-09-04 08:29:58');
INSERT INTO `m_mutasi` VALUES (27, '6313929159D4B', 'INV/IX/2022/002', '2022-09-23', 'keluar', 26000, 28860, '03-09-2022', 'dadad', '2022-09-03 17:45:05', '2022-09-04 08:29:01');
INSERT INTO `m_mutasi` VALUES (28, '6314A14158666', NULL, '2022-09-07', 'keluar', 20000, 22200, '04-09-2022', 'order', '2022-09-04 13:00:28', '2022-09-04 13:00:28');
INSERT INTO `m_mutasi` VALUES (29, '6314A39235AF1', 'INV/IX/2022/003', '2022-09-04', 'keluar', 250000, 277500, '04-09-2022', 'BELI', '2022-09-04 13:10:50', '2022-09-04 13:12:31');
INSERT INTO `m_mutasi` VALUES (30, '6314A84EF0DB9', 'INV/IX/2022/003', '2022-09-06', 'keluar', 80000, 88800, '04-09-2022', 'beli', '2022-09-04 13:31:01', '2022-09-04 13:32:08');

-- ----------------------------
-- Table structure for trx_mutasi_kode_barang
-- ----------------------------
DROP TABLE IF EXISTS `trx_mutasi_kode_barang`;
CREATE TABLE `trx_mutasi_kode_barang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_bukti` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah_order` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_mutasi_kode_barang
-- ----------------------------
INSERT INTO `trx_mutasi_kode_barang` VALUES (154, '63137DDF9D320', 'XHA-FAIAF-AFAJAFJF', 'Beras', '10', '2022-09-03 16:16:43', '2022-09-03 16:16:48');
INSERT INTO `trx_mutasi_kode_barang` VALUES (155, '631390EB2F73E', 'XBCV-ADHIJ-DAKJJDA', 'Sabun Mandi', '13', '2022-09-03 17:37:52', '2022-09-03 17:38:01');
INSERT INTO `trx_mutasi_kode_barang` VALUES (156, '6313929159D4B', 'XBCV-ADHIJ-DAKJJDA', 'Sabun Mandi', '13', '2022-09-03 17:44:54', '2022-09-03 17:45:05');
INSERT INTO `trx_mutasi_kode_barang` VALUES (157, '6314A14158666', 'XYRY-HHBK-BJ', 'Sapu', '5', '2022-09-04 13:00:10', '2022-09-04 13:00:28');
INSERT INTO `trx_mutasi_kode_barang` VALUES (158, '6314A39235AF1', 'GFYYUG-HUGYFY-HUGY', 'GAS ELPIJI', '10', '2022-09-04 13:10:26', '2022-09-04 13:10:50');
INSERT INTO `trx_mutasi_kode_barang` VALUES (159, '6314A40E2E186', 'XHHUH-IUIF-GFUHIB', 'Detergent', NULL, '2022-09-04 13:11:47', '2022-09-04 13:11:47');
INSERT INTO `trx_mutasi_kode_barang` VALUES (160, '6314A40E2E186', 'XHHUH-IUIF-GFUHIB', 'Detergent', NULL, '2022-09-04 13:11:47', '2022-09-04 13:11:47');
INSERT INTO `trx_mutasi_kode_barang` VALUES (161, '6314A40E2E186', 'XYRY-HHBK-BJ', 'Sapu', NULL, '2022-09-04 13:11:52', '2022-09-04 13:11:52');
INSERT INTO `trx_mutasi_kode_barang` VALUES (162, '6314A40E2E186', 'XHA-FAIAF-AFAJAFJF', 'Beras', NULL, '2022-09-04 13:11:57', '2022-09-04 13:11:57');
INSERT INTO `trx_mutasi_kode_barang` VALUES (163, '6314A84EF0DB9', 'NDJAIJ-DAKB-ADADHSA', 'Kasuur Tidur', '10', '2022-09-04 13:30:19', '2022-09-04 13:31:01');
INSERT INTO `trx_mutasi_kode_barang` VALUES (164, '6314A8ABEA27A', 'XHHUH-IUIF-GFUHIB', 'Detergent', NULL, '2022-09-04 13:31:31', '2022-09-04 13:31:31');
INSERT INTO `trx_mutasi_kode_barang` VALUES (165, '6314A8ABEA27A', 'GFYYUG-HUGYFY-HUGY', 'GAS ELPIJI', NULL, '2022-09-04 13:31:35', '2022-09-04 13:31:35');
INSERT INTO `trx_mutasi_kode_barang` VALUES (166, '6314A8ABEA27A', 'XHA-FAIAF-AFAJAFJF', 'Beras', NULL, '2022-09-04 13:31:39', '2022-09-04 13:31:39');

SET FOREIGN_KEY_CHECKS = 1;
