<?php

use App\Http\Controllers\AccountsController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MutasiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return redirect()->route('beranda');
    return view ('login');
})->name('login');
//Ajax Function
Route::get('/delete-barang/{id}', [BarangController::class, 'delete_barang'])->name('delete-barang');
Route::get('/edit-form-barang/{id}',[BarangController::class, 'form_edit_barang'])->name('form-edit-barang');
Route::get('/save-kode-barang/{id}/{kode}', [MutasiController::class, 'save_kode_barang'])->name('save-kode-barang');
Route::get('/delete-kode-barang/{id}', [MutasiController::class, 'delete_kode_barang'])->name('delete-kode-barang');


//Function Barang
Route::get('/beranda',[BerandaController::class,'beranda'])->name('beranda');
Route::get('/create-new-barang',[BarangController::class, 'barang'])->name('create-barang');
Route::get('/cek-kode-barang/{id}', [BarangController::class, 'cek_kode_barang'])->name('cek_kode_barang');
Route::post('/save-new-barang', [BarangController::class, 'create_barang'])->name('save-barang');
Route::get('/browse-barang', [BarangController::class, 'browse_barang'])->name('browse-barang');
Route::post('/save_update_barang', [BarangController::class, 'save_update_barang'])->name('save-update-barang');


//Function Mutasi
Route::get('/create-mutasi',[MutasiController::class, 'create_mutasi'])->name('create-mutasi');
Route::post('/save-mutasi', [MutasiController::class, 'save_mutasi'])->name('save-mutasi');
Route::get('/report-mutasi', [MutasiController::class, 'report_mutasi'])->name('report-mutasi');
Route::get('/get-barang-mutasi/{id}', [MutasiController::class, 'get_barang_mutasi'])->name('get-barang-mutasi');
Route::get('/delete-mutasi/{id}', [MutasiController::class, 'delete_mutasi'])->name('delete-mutasi');
Route::get('/update-mutasi/{id}', [MutasiController::class, 'update_mutasi'])->name('update-mutasi');
Route::post('/save-update-mutasi', [MutasiController::class, 'save_update_mutasi'])->name('save-update-mutasi');
Route::get('/delete-item-mutasi/{id}', [MutasiController::class, 'delete_item_mutasi'])->name('delete-item-mutasi');


//accounts
Route::get('/accounts-setting',[AccountsController::class, 'accounts'])->name('accounts-setting');
Route::post('/login-settinng', [AccountsController::class, 'login'])->name('login-setting');
Route::post('/save-accounts', [AccountsController::class, 'save_accounts'])->name('save-accounts');
Route::get('/delete-user/{id}', [AccountsController::class, 'delete_user'])->name('delete-accounts');



///invoice
Route::get('/invoice/{id}',[InvoiceController::class,'get_invoice'])->name('get-invoice');













