@extends('main-page.main')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="{{URL::asset('backend')}}/css/pages/toastify.css">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<style>
    body {
        /* font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; */
        /* text-align: center; */
        color: #777;
    }

    body h1 {
        font-weight: 300;
        margin-bottom: 0px;
        padding-bottom: 0px;
        color: #000;
    }

    body h3 {
        font-weight: 300;
        margin-top: 10px;
        margin-bottom: 20px;
        /* font-style: italic; */
        color: #555;
    }

    body a {
        color: #06f;
    }

    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
        border-collapse: collapse;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
</style>

<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color:white ;">Report Data Mutasi PT Wood Interga</h3>
                    <p class="text-subtitle" style="color: white;">Berikut adalah beberapa Data yang diperoleh sistem!!</p>
                </div>
            </div>
        </div>


        <section class="section">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Invoice : {{$invoice->invoice}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="invoice-box">
                                <table>
                                    <tr class="top">
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td class="title">
                                                        <img src="https://www.ptppi.co.id/wp-content/uploads/2022/03/BGRLI.png" alt="Company logo" style="width: 100%; max-width: 300px" />
                                                    </td>

                                                    <td>
                                                        Invoice #: {{$invoice->invoice}}<br />
                                                        Created: {{$invoice->tanggal}}<br />
                                                        SKU: {{$invoice->no_bukti}}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr class="heading">
                                        <td>Payment Method</td>

                                        <td>Check #</td>
                                    </tr>

                                    <tr class="details">
                                        <td>Cash</td>

                                        <td>CASH PAYMENT</td>
                                    </tr>

                                    <tr class="heading">
                                        <td>Item</td>

                                        <td>Harga</td>
                                    </tr>
                                    <tr class="item">
                                        @foreach ($invoice_detail as $detail)
                                        @php
                                        $harga = App\Models\BarangModel::where('kode_barang',$detail->kode_barang)->first();
                                        @endphp

                                        <td>{{$detail->nama_barang}}</td>

                                        <td> {{number_format($harga->harga_satuan * $detail->jumlah_order,2,',','.')}} </td>
                                        @endforeach
                                    </tr>

                                    <tr class="item">
                                        <td>Harga Sebelum Pajak</td>

                                        <td>{{number_format($invoice->harga_normal,2,',','.')}}</td>
                                    </tr>

                                    <tr class="item">
                                        <td>Biaya Pajak</td>

                                        <td>11%</td>
                                    </tr>
                                    <tr class="item">
                                        <td>Harga Setelah Pajak</td>

                                        <td>{{number_format($invoice->harga_pajak,2,',','.')}}</td>
                                    </tr>

                                    <tr class="item last">
                                        <td>Total Dibayar</td>

                                        <td>{{number_format($invoice->harga_pajak,2,',','.')}}</td>
                                    </tr>

                                    <tr class="total">
                                        <td></td>

                                        <td>Total: {{number_format($invoice->harga_pajak,2,',','.')}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

</div>




@endsection
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    function alert_hapus() {
        Swal.fire(
            'Deleted!',
            'Barang Berhasil Dihapus.',
            'success'
        )
    }

    function alert_ditambah() {
        Swal.fire(
            'Saved!',
            'Barang Berhasil Diupdated.',
            'success'
        )
    }
</script>
<script>
    $(document).ready(function() {
        $('#btn-tambah-user').click(function() {
            $('#table_user').hide();
            $('#form_tambah').show();
        });
        $('#btn-back').click(function() {
            $('#table_user').show();
            $('#form_tambah').hide();
        });
        $('#btn-hapus').click(function() {
            console.log('hapus');
        });
        $('#tgl_awal, #tgl_akhir').change(function() {
            var tgl_awal = $('#tgl_awal').val();
            var tgl_akhir = $('#tgl_akhir').val();
            console.log(tgl_awal + '-' + tgl_akhir)
            $('#btn-filter').html('<a href="/report-mutasi?tgl_awal=' + tgl_awal + '&tgl_akhir=' + tgl_akhir + '"><span class="btn btn-primary">Filter</button></a>')



        });


    })
</script>
<script>
    $(function() {

        var sort = $('#kecamatan');


        function loadDataTable() {

            $('#data_mutasi').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                scrollX: true,
                order: [
                    [5, 'DESC']
                ],

                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        render: function(data) {
                            return '<span class="badge bg-primary">' + data + '</span>';
                        }
                    },

                    {
                        data: 'no_bukti',
                        name: 'no_bukti',
                        render: function(data) {
                            return '<span class="badge bg-success">' + data + '</span>';
                        }
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal',
                        render: function(data) {
                            return data.toUpperCase();
                        }
                    },
                    {
                        data: 'indikator',
                        name: 'indikator',
                        render: function(data) {
                            return '<span class="badge bg-primary">' + data.toUpperCase() + '</span>';
                        }
                    },
                    {
                        data: 'nama_barang',
                        name: 'nama_barang',

                    },
                    {
                        data: 'in',
                        name: 'in',
                        render: function(data) {
                            return data;
                        }
                    },
                    {
                        data: 'out',
                        name: 'out',
                        render: function(data) {
                            return data;
                        }
                    },
                    {
                        data: 'id',
                        name: 'id',
                        render: function(data) {
                            return ' <a href="#" data-toggle="modal" data-target="#exampleModalCenter' + data + '"><span class="badge bg-secondary">Lihat Keteranga</span></a>';
                        }

                    },
                    {
                        data: 'action',
                        name: 'action',
                    }


                ]
            });

        }

        loadDataTable();

        kecamatan.change(function() {
            // alert('ok')
            loadDataTable();
        });



    });
</script>