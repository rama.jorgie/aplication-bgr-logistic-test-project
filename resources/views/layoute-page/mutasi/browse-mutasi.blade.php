@extends('main-page.main')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="{{URL::asset('backend')}}/css/pages/toastify.css">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color:white ;">Report Data Penjualan Report</h3>
                    <p class="text-subtitle" style="color: white;">Berikut adalah beberapa Data yang diperoleh sistem!!</p>
                </div>
            </div>
        </div>
        @if ($message = Session::get('hapus'))
        <script>
            alert_hapus();
        </script>
        @endif
        @if ($message = Session::get('ditambah'))
        <script>
            alert_ditambah();
        </script>
        @endif
        <section class="section" id="table_user">
            <div class="row" id="table-head">
                <div class="col-12">
                    <div class="card">
                        <table style="margin-top: 20px;">
                            <tr style="text-align:center ;">
                                <td><img src="https://www.ptppi.co.id/wp-content/uploads/2022/03/BGRLI.png" width="100px" alt="" srcset=""></td>
                                <td>
                                    <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Report Data Mutasi</h4>
                                    <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Di PT BGR LOGISTIC Indonesia</h4>
                                </td>
                                <td><img src="https://www.ptppi.co.id/wp-content/uploads/2022/03/BGRLI.png" width="100px" alt="" srcset=""></td>
                            </tr>
                        </table>
                        <hr>
                        <div class="card-content p-3">
                            <!-- <form action="{{route('report-mutasi')}}?tgl=123" method="get"> -->
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="disabledInput">Tanggal Awal</label>
                                            <input type="date" class="form-control" id="tgl_awal" name="tgl_awal">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="disabledInput">Tanggal Akhir</label>
                                            <input type="date" class="form-control" id="tgl_akhir" name="tgl_akhir">
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="disabledInput">&nbsp;</label><br>
                                            <!-- <button id="btn-filter" class="btn btn-primary">Filter</button> -->
                                            <div id="btn-filter"></div>

                                        </div>
                                    </div>
                                    <!-- </form> -->
                                    <div class="col-md-3">
                                        <div style="text-align: right;margin-bottom: 5px;margin-right: 5px;">
                                            <a href="{{route('create-mutasi')}}"><button class="btn btn-primary">Tambah Mutasi</button></a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <table id="data_mutasi" class="display  nowrap" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>SKU</th>
                                        <th>Tanggal</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Harga Sebelum Pajak</th>
                                        <th>Harga Sesudah Pajak</th>
                                        <th>Keterangan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

</div>


@foreach ($mutasi as $value3)
<div class="modal fade" id="exampleModalCenter{{$value3->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Keterangan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" style="color: black ;">
                    <div class="form-group">
                        <textarea class="form-control" name="keterangan" id="exampleFormControlTextarea1" rows="3">{{$value3->keterangan}}</textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endforeach



@endsection
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    function alert_hapus() {
        Swal.fire(
            'Deleted!',
            'Barang Berhasil Dihapus.',
            'success'
        )
    }

    function alert_ditambah() {
        Swal.fire(
            'Saved!',
            'Barang Berhasil Diupdated.',
            'success'
        )
    }
</script>
<script>
    $(document).ready(function() {
        $('#btn-tambah-user').click(function() {
            $('#table_user').hide();
            $('#form_tambah').show();
        });
        $('#btn-back').click(function() {
            $('#table_user').show();
            $('#form_tambah').hide();
        });
        $('#btn-hapus').click(function() {
            console.log('hapus');
        });
        $('#tgl_awal, #tgl_akhir').change(function() {
            var tgl_awal = $('#tgl_awal').val();
            var tgl_akhir = $('#tgl_akhir').val();
            console.log(tgl_awal + '-' + tgl_akhir)
            $('#btn-filter').html('<a href="/report-mutasi?tgl_awal=' + tgl_awal + '&tgl_akhir=' + tgl_akhir + '"><span class="btn btn-primary">Filter</button></a>')



        });


    })
</script>
<script>
    $(function() {

        var sort = $('#kecamatan');


        function loadDataTable() {

            $('#data_mutasi').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                scrollX: true,
                order: [
                    [5, 'DESC']
                ],

                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        render: function(data) {
                            return '<span class="badge bg-primary">' + data + '</span>';
                        }
                    },

                    {
                        data: 'no_bukti',
                        name: 'no_bukti',
                        render: function(data) {
                            return '<span class="badge bg-success">' + data + '</span>';
                        }
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal',
                        render: function(data) {
                            return data.toUpperCase();
                        }
                    },
                    {
                        data: 'nama_barang',
                        name: 'nama_barang',
                        render: function(data) {
                            return data.toUpperCase();
                        }
                    },
                    {
                        data: 'qty',
                        name: 'qty',
                       
                    },
                   
                    {
                        data: 'harga_normal',
                        name: 'harga_normal',

                    },
                    // 
                    {
                        data: 'harga_pajak',
                        name: 'harga_pajak',
                        render: function(data) {
                            return data;
                        }
                    },
                    
                    
                    {
                        data: 'id',
                        name: 'id',
                        render: function(data) {
                            return ' <a href="#" data-toggle="modal" data-target="#exampleModalCenter' + data + '"><span class="badge bg-secondary">Lihat Keteranga</span></a>';
                        }

                    },
                    {
                        data: 'action',
                        name: 'action',
                    }


                ]
            });

        }

        loadDataTable();

        kecamatan.change(function() {
            // alert('ok')
            loadDataTable();
        });



    });
</script>