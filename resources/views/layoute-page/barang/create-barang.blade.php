@extends('main-page.main')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="{{URL::asset('dashboard')}}/css/pages/toastify.css">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>
    @if ($message = Session::get('berhasil'))
    <script>
        alert_add();
    </script>
    @endif

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color:white ;">Form Tambah Data Barang (Master)</h3>
                    <p class="text-subtitle" style="color: white;">Berikut adalah data yang diperoleh sistem! </p>
                </div>
            </div>
        </div>

        <section class="section" id="">
            <div class="card">
                <table style="margin-top: 20px;">
                    <tr style="text-align:center ;">
                        <td><img src="https://www.ptppi.co.id/wp-content/uploads/2022/03/BGRLI.png" width="100px" alt="" srcset=""></td>
                        <td>
                            <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Form Tambah Data Barang</h4>
                            <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Di PT BGR LOGISTIC Indonesia</h4>
                        </td>
                        <td><img src="https://www.ptppi.co.id/wp-content/uploads/2022/03/BGRLI.png" width="100px" alt="" srcset=""></td>

                    </tr>
                </table>
                <hr>
                <div class="card-body">
                    <div style="text-align:right ;">
                        <a href="{{route('browse-barang')}}"><button class="btn btn-success">Lihat Barang Tersimpan</button></a>
                    </div>
                    <form id="form_post" action="{{route('save-barang')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label for="basicInput">Kategori Barang</label>
                                    <input type="text" class="form-control" id="kategori" name="kategori" value="Barang" readonly>
                                </div>
                                <div class=" form-group">
                                    <label for="helpInputTop">Nama Barang</label>
                                    <input type="text" class="form-control" id="nama_barang" name="nama_barang" required>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="row">
                                    <div class=" form-group">
                                        <label for="helperText">Kode Barang</label>
                                        <input type="text" class="form-control" id="kode_barang" name="kode_barang" required>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group ">
                                            <label for="disabledInput">Jumlah Barang</label>
                                            <input type="number" class="form-control" id="jumlah_barang" name="jumlah_barang">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label for="disabledInput">&nbsp;</label>
                                            <input type="text" class="form-control" value="UoM/Pcs" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="disabledInput">&nbsp;</label><br>
                                            <span id="btn-cek" class="btn btn-primary">✓ Cek Data</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label for="basicInput">Harga Satuan</label>
                                    <input type="number" class="form-control" id="kategori" name="harga_satuan" required>
                                </div>
                               
                            </div> <br>
                            <div id="btn-pre-save" style="text-align:center ; ">
                                <span class="badge bg-warning">Silahkan Lakukan Cek Data Terlebih Dahulu !!!</span>
                                <!-- <button id="btn-back" class="btn btn-danger">Kembali</button> -->
                            </div>
                            <div id="btn-save" style="text-align:center ;display: none; ">
                                <!-- <button id="btn-back" class="btn btn-danger">Kembali</button> -->
                                <button id="simpan" class="btn btn-success">Simpan</button>
                                <!-- <button id="btn-back" class="btn btn-danger">Kembali</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    @include('main-page.footer')
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="{{URL::asset('dashboard')}}/js/extensions/toastify.js"></script>
<script>
    function alert_add() {
        Swal.fire(
            'Saved!',
            'Barang Berhasil Disimpan.',
            'success'
        )
    }
</script>
<script>
    $(document).ready(function() {

        $('#kode_barang').on('keyup', function() {
            var url = $('#kode_barang').val();
            var rep = url.replaceAll(' ', '-');
            // console.log(rep);
            $('#kode_barang').val(rep);
        });

        $('#btn-cek').click(function() {
            // console.log('cek')
            var kode_barang = $('#kode_barang').val();
            var nama_barang = $('#nama_barang').val();
            var jumlah_barang = $('#jumlah_barang').val();
            console.log(nama_barang)
            if (kode_barang == '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Harap Lengkapi Data',
                })
            }

            //  console.log(kode_barang)
            $.ajax({
                url: '/cek-kode-barang/' + kode_barang,
                type: "get",
                success: function(data) {
                    console.log(data)
                    if ((nama_barang == '') || (jumlah_barang == '')) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Harap Lengkapi Data',
                        })
                    } else {
                        if (data == 0) {

                            Swal.fire(
                                'Kode Barang Dapat Digunakan!',
                                'Silahkan Melanjutkan!',
                                'success'
                            )
                            $('#kode_barang').prop('readonly', true);
                            $('#btn-pre-save').hide();
                            $('#btn-save').show();

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Kode Barang Sudah Digunakan!',
                            })
                        }
                    }

                }
            });

        });

    })
</script>