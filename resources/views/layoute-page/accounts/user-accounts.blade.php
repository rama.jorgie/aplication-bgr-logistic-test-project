@extends('main-page.main')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="{{URL::asset('backend')}}/css/pages/toastify.css">


<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color:white ;">Data Status Accoutns User</h3>
                    <p class="text-subtitle" style="color: white;">Berikut adalah beberapa user yang terdaftar </p>
                </div>
            </div>
        </div>
        @if ($message = Session::get('gagal'))
        <div class="toastify on  toastify-right toastify-top" style="color: red; transform: translate(0px, 0px); top: 15px;">Username Sudah Digunakan<span class="toast-close">✖</span></div>
        @endif
        <section class="section" id="table_user">
            <div class="row" id="table-head">
                <div class="col-12">
                    <div class="card">

                        <table style="margin-top: 10px;">
                            <tr style="text-align:center ;">
                                <td><img src="https://www.ptppi.co.id/wp-content/uploads/2022/03/BGRLI.png" width="100px" alt="" srcset=""></td>
                                <td>
                                    <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">List Data User Terdaftar </h4>
                                    <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Di PT BRG LOGISTIC Indonesia</h4>
                                </td>
                                <td><img src="https://www.ptppi.co.id/wp-content/uploads/2022/03/BGRLI.png" width="100px" alt="" srcset=""></td>

                            </tr>
                        </table>
                        <hr>

                        <div style="text-align: right; margin-right: 15px;">
                            <button id="btn-tambah-user" class="btn btn-primary">Tambah User</button>

                        </div>
                        <div class="card-content p-3">
                            <table id="data_user" class="display  nowrap" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Level</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section" style="display:none ;" id="form_tambah">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><button id="btn-back" class="btn btn-danger">Kembali</button> Form Pendaftaran User Baru </h4>
                    <hr>

                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-6">

                            <form id="form_post" method="post" action="{{route('save-accounts')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="basicInput">Pilih Level</label>
                                    <select class="form-select " name="level" id="sekolah" style="width: 100%;" required>
                                        <option value="kasir">Kasir</option>
                                        <option value="admin_stock">Admin Stock</option>
                                    </select>
                                </div>
                                <!-- <div class="form-group">
                                    <label for="helpInputTop">Pilih Sekolah</label>
                                    <small class="text-muted">eg.<i>someone@example.com</i></small>
                                    <input type="text" class="form-control" name="sekolah" id="sekolah" placeholder="ketik Nama Sekolah" required>
                                </div> -->

                                <div class=" form-group">
                                    <label for="helpInputTop">Username</label>
                                    <small class="text-muted">eg.<i>someone@example.com</i></small>
                                    <input type="text" class="form-control" id="username" name="username" required>
                                </div>

                                <!-- <div class=" form-group">
                                    <label for="disabledInput">NIK</label>
                                    <input type="text" class="form-control" id="url" name="nik">
                                </div> -->
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="disabledInput">Tanggal Register</label>
                                <input type="text" class="form-control" id="readonlyInput" readonly name="tanggal" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class=" form-group">
                                        <label for="helperText">Password</label>
                                        <input type="password" id="password" class="form-control" placeholder="password" name="password" required>
                                    </div>
                                </div>

                            </div>
                            <!-- <small class="text-muted" id="smal-test">URL <i>someone@example.com</i></small> -->

                            <!-- <div class="form-group">
                                <label for="disabledInput">Nama</label>
                                <input type="text" class="form-control" name="nama" required>
                            </div> -->

                        </div> <br>
                        <div id="btn-save" style="text-align:center ;">
                            <button id="simpan" type="submit" class="btn btn-success">Simpan</button>
                            <!-- <button id="btn-back" class="btn btn-danger">Kembali</button> -->
                        </div>
                        </form>
                        <!-- <div style="text-align:center ;"> -->
                        <!-- <button id="simpan" type="submit" class="btn btn-success">Simpan</button> -->
                        <!-- </div> -->
                    </div>

                </div>
            </div>
        </section>
    </div>

</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
    $(document).ready(function() {
        $('#url_custom').on('keyup', function() {
            var url = $('#url_custom').val();
            var rep = url.replaceAll(' ', '-');
            // console.log(rep);
            $('#url_custom').val(rep);
        });
        $('#cek_url2').on('click', function() {
            var username = $('#username').val();
            console.log(username);
            $.ajax({
                url: '/cek-username/' + username,
                type: 'get',
                success: function(data) {
                    console.log(data);
                    if (data == 0) {
                        // $('#username').val('');
                        Swal.fire(
                            'Good job!',
                            'You clicked the button!',
                            'success'
                        ), 3000;
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Username Sudah Digunakan',
                        })
                    }
                }
            });
        });

    });
</script>
<script>
    $(document).ready(function() {
        $('#btn-tambah-user').click(function() {
            $('#table_user').hide();
            $('#form_tambah').show();
        });
        $('#btn-back').click(function() {
            $('#table_user').show();
            $('#form_tambah').hide();
        });
    })
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('select[name="sekolah"]').on('change', function() {
            var sekolah = jQuery(this).val();
            // console.log(sekolah);
            var myarr = sekolah.split("-");
            var lowercase = myarr[1].toLowerCase();
            var replace1 = lowercase.replace(" ", "-");
            var replace2 = replace1.replace(" ", "-");

            console.log(replace2);

            jQuery.ajax({
                url: '/count-slug/' + replace2,
                type: "GET",
                dataType: "html",
                success: function(data) {
                    if (data != 'slug tersedia') {
                        // jQuery('#url_web').val(replace2);
                        $('#btn-save').hide();
                        // // Swal.fire({
                        // //     icon: 'error',
                        // //     title: 'Oops...',
                        // //     text: 'Url Default Tidak dapat digunakan!',
                        // //     footer: '<a href="#">Url Akan Dirandom, Silahkan Melakukan Mengupdate</a>'
                        // })
                        if (sekolah) {
                            $('#url_web').val('http://sekolah.mojokertokab.go.id/');
                            $('#url_custom').val(replace2);
                            $('#smal-test').val(replace2);

                        } else {
                            $('#url_web').val('http://sekolah.mojokertokab.go.id/');
                            $('#url_custom').empty();
                            $('#smal-test').val('http://sekolah.mojokertokab.go.id/');
                        }
                    } else {
                        // Swal.fire({
                        //     position: 'top-end',
                        //     icon: 'success',
                        //     title: 'Url Dapat Digunakan',
                        //     showConfirmButton: false,
                        //     timer: 1500
                        // })
                        // $('#btn-save').show();
                        $('#btn-save').hide();
                        $('#url_custom').prop('readonly', false);


                        if (sekolah) {
                            $('#url_web').val('http://sekolah.mojokertokab.go.id/');
                            $('#url_custom').val(replace2);

                        } else {
                            $('#url_web').val('http://sekolah.mojokertokab.go.id/');
                            $('#url_custom').empty();
                        }
                    }

                }
            });


        });
    });
</script>
<script>
    jQuery(document).ready(function() {
        $('#cek_url').click(function() {
            var url = $('#url_custom').val();
            jQuery.ajax({
                url: '/count-slug/' + url,
                type: "GET",
                dataType: "html",
                success: function(data) {
                    if (data != 'slug tersedia') {
                        // jQuery('#url_web').val(replace2);
                        $('#btn-save').hide();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Url Default Tidak dapat digunakan!',
                            footer: '<a href="#">Url Akan Dirandom, Silahkan Melakukan Mengupdate</a>'
                        })
                    } else {
                        // Swal.fire({
                        //     position: 'top-end',
                        //     icon: 'success',
                        //     title: 'Url Dapat Digunakan',
                        //     showConfirmButton: false,
                        //     timer: 3000
                        // })
                        var username = $('#username').val();
                        console.log(username);
                        $.ajax({
                            url: '/cek-username/' + username,
                            type: 'get',
                            success: function(data) {
                                console.log(data);
                                if (data == 0) {
                                    // $('#username').val('');
                                    Swal.fire(
                                        'Kerja Baguus!',
                                        'Username & Url Dapat digunakan!',
                                        'success'
                                    )
                                    // $('$url_custom').setAttribute("readonly", true);
                                    $('#btn-save').show();
                                    $('#url_custom').prop('readonly', true);

                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Username Sudah Digunakan',
                                    })
                                }
                            }
                        });
                    }

                }
            });
        });
    });
</script>
<script>
    $(function() {

        var kecamatan = $('#kecamatan');


        function loadDataTable() {

            $('#data_user').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                scrollX: true,
                order: [
                    [5, 'DESC']
                ],

                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',

                    },
                    {
                        data: 'username',
                        name: 'username',
                        render: function(data) {
                            return '<span class="badge bg-primary">' + data + '</span>';
                        }

                    },
                    {
                        data: 'level',
                        name: 'level',
                        render: function(data) {
                            return '<span class="badge bg-success">' + data + '</span>';
                        }

                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        render: function(data) {
                            return '<span class="badge bg-info"> 04-09-2022 </span>';
                        }

                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                        render: function(data) {
                            return '<span class="badge bg-info">04-09-2022</span>';
                        }
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },

                ]
            });

        }

        loadDataTable();

        kecamatan.change(function() {
            // alert('ok')
            loadDataTable();
        });



    });
</script>